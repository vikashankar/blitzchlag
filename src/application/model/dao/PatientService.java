package application.model.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import application.dto.PatientDTO;

@Repository
public class PatientService 
{
	public PatientService () {
		System.out.println(this.getClass().getName()+ " created..");
	}
	
	@Autowired
	private SessionFactory factory;
	
	public void save(PatientDTO dto)
	{
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		
		try {
			session.save(dto);
			transaction.commit();
		}
		catch (HibernateException He) {
			He.printStackTrace();
		}
		finally {
			session.close();
		}
	}
}
