package application.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.DispatcherServlet;

import application.dto.PatientDTO;
import application.model.dao.PatientService;

public class Service {
public Service() {
	System.out.println(this.getClass().getName());
}
@Autowired
private PatientService dao;
public void service(PatientDTO dto)
{
	System.out.println("calling create in service..." + dto);
	if(dto!=null)
	{
		System.out.println("valid data");
		dao.save(dto);
		return;
	}
	else{
		System.out.println("invalid data");
	}
}

}
