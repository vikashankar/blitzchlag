package application.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="login")
public class PatientDTO implements Serializable 
{
	public PatientDTO() {
		System.out.println(this.getClass().getName());
	}
	@Column(name="pid")
	int PatientID;
	@Column(name="pname")
	String PatientName;
	@Column(name="pnum")
	long PhoneNumber;
	@Column(name="padhar")
	long PatientAdharNo;
	@Column(name="doctor")
	String ConsultingDoctor;
	@Column(name="age")
	int Age;
	@Column(name="dob")
	String DateOfBirth;
	@Column(name="refered")
	String ReferedBy;
	@Column(name="occupation")
	String Occupation;
	@Column(name="country")
	String Country;
	@Column(name="state")
	String State;
	String City;
	
	public int getPatientID() {
		return PatientID;
	}
	public void setPatientID(int patientID) {
		PatientID = patientID;
	}
	public String getPatientName() {
		return PatientName;
	}
	public void setPatientName(String patientName) {
		PatientName = patientName;
	}
	public long getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	public long getPatientAdharNo() {
		return PatientAdharNo;
	}
	public void setPatientAdharNo(long patientAdharNo) {
		PatientAdharNo = patientAdharNo;
	}
	public String getConsultingDoctor() {
		return ConsultingDoctor;
	}
	public void setConsultingDoctor(String consultingDoctor) {
		ConsultingDoctor = consultingDoctor;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		Age = age;
	}
	public String getDateOfBirth() {
		return DateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}
	public String getReferedBy() {
		return ReferedBy;
	}
	public void setReferedBy(String referedBy) {
		ReferedBy = referedBy;
	}
	public String getOccupation() {
		return Occupation;
	}
	public void setOccupation(String occupation) {
		Occupation = occupation;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getPaymentType() {
		return PaymentType;
	}
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	String PaymentType;
	@Override
	public String toString() {
		return "RegisterDTO [PatientID=" + PatientID + ", PatientName=" + PatientName + ", PatientAdharNo=" + PatientAdharNo + ", ConsultingDoctor=" + ConsultingDoctor
				+ ", Age=" + Age + ", DateOfBirth=" + DateOfBirth+ ", ReferedBy=" + ReferedBy
				+ ", Occupation=" + Occupation + ", Country=" + Country + ", State=" + State + "]";
}
}
