package application.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import application.dto.PatientDTO;
import application.model.service.Service;

@Controller
@RequestMapping("/")
public class Patientcontroller
{
	public Patientcontroller() {
		System.out.println(this.getClass().getName() +" created");
	}
	
	@Autowired
	private Service service;
	
	@RequestMapping(value="register.do",method=RequestMethod.POST)
	public ModelAndView loginService(PatientDTO dto) {
		System.out.println("service method Entering");
		service.service(dto);
		System.out.println("service method closing");
		return new ModelAndView("Data Stored sucessfully");
		
	}
}
